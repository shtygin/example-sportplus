export type StaticOrigin = boolean | string | RegExp | (boolean | string | RegExp)[]

export type OriginFn = (
    origin: string | undefined,
    req: Request
) => StaticOrigin | Promise<StaticOrigin>

export interface CorsOptions {
    origin?: StaticOrigin | OriginFn
    methods?: string | string[]
    allowedHeaders?: string | string[]
    exposedHeaders?: string | string[]
    credentials?: boolean
    maxAge?: number
    preflightContinue?: boolean
    optionsSuccessStatus?: number
}