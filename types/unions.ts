export type COMMON_TYPES =
    | 'active'
    | 'open'
    | 'modal-open'
    | 'aside-open'

export type TABS_TYPES =
    | 'LIVE'
    | 'TODAY'
    | 'LATER'

export type PERIODS_TYPES =
    | 'SET'
    | 'HALF'
    | 'QUARTER'

export type FETCH_TYPES =
    | 'SUCCESS'
    | 'FAILED'
    | 'PENDING'

export type LOCALES_TYPES =
    | 'EN'
    | 'RU'
    | 'FR'
    | 'NL'
    | 'DE'

export type PAGES_STATIC_TYPES =
    | 'BOOKMAKERS'
    | 'FAVORITE'
    | 'BONUSES'
    | 'SUPPORT'
    | 'COPYRIGHT'
    | 'HOME'

export type PAGES_STATIC_ROUTES_TYPES =
    | '/bookmakers/promo/'
    | '/favorite/'
    | '/bonuses/'
    | '/support/'
    | '/copyright/'
    | '/'