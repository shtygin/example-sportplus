import type {Immutable} from "@/types/basic";
import type {NextApiRequest, NextApiResponse} from "next";

type CS = {
    not_started: number
    live: number
}

export type TournamentProps = {
    key?: number
    id: number
    name: string
    alias?: string
    tournamentURI: string | number
    priority: number
    total: number
    cs: CS
}

export type TranslationsProps = {
    isLoading: boolean
    isError: Error | boolean
    data: (SportProps[] | null) | (JSON | null)
}

export type LinksGroupProps = {
    group: [{
        id: number
        name: string
        sportURI: number & URL
    }]
}

export type SportProps = {
    key?: number
    onClick?: Function
    tournaments: TournamentProps[]
    tournamentsLive: TournamentProps[]
    sportURI: string
    total: number
    totalLive: number
    readonly alias: string
    readonly uri: string | number
    readonly id: number
    readonly name: string
    readonly priority: number
    readonly has_more?: boolean
}

export type ScrollProps = {
    behavior: | 'smooth',
    block: | 'center'
}

export type MiddlewareProps = Immutable<{
    req?: NextApiRequest,
    res: NextApiResponse,
    fn?: Function,
}>