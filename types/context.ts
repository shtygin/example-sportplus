import type {TABS_TYPES} from "@/types/unions";
import type {Immutable, SetState} from "./basic";
import type {CookieSerializeOptions} from "cookie";
import type {MiddlewareProps} from "@/types/props";
import type {Context} from "react";

// type        activeSport: Required<{ id: number, name: string }>,
//     setActiveSport: SetState<object>
//
// activeTournament: Required<{ id: number, name: string }>,
//     setActiveTournament: SetState<object>
//
// activeGame: string,
//     setActiveGame: SetState<string>
//
// activeTab: TABS_TYPES | null,
//     setActiveTab: SetState<TABS_TYPES> | SetState<null>,
//
//
//     favorite: [] | never[],
//     setFavorite: SetState<[]> | SetState<never[]>
// timezone: number | null,
//     setTimezone: SetState<number> | SetState<null>
// time: string
// setTime: SetState<string>
//
// sportContext: {
// },
// tournamentContext: {
//
// },
// gameContext: {
//
// },
// tabContext: {
//
// },
// favoriteContext: {
//
// },
// timezoneContext: {
//
// },
// timeContext: {
//
// },

type ActiveSportProps = string
type ActiveTournamentProps = string
type ActiveGameType = string
type ActiveTabType = Lowercase<TABS_TYPES>
type FavoriteType = []

type SportSetter = SetState<ActiveSportProps>
type TournamentSetter = SetState<ActiveTournamentProps>
type GameSetter = SetState<ActiveGameType>
type TabSetter = SetState<ActiveTabType>
type FavoriteSetter = SetState<FavoriteType>

type SportContext = { activeSport: ActiveSportProps, setActiveSport: SportSetter }
type TournamentContext = { activeTournament: ActiveTournamentProps, setActiveTournament: TournamentSetter }
type GameContext = { activeGame: ActiveGameType, setActiveGame: GameSetter }
type TabContext = { activeTab: ActiveTabType, setActiveTab: TabSetter }
type FavoriteContext = { favorite: FavoriteType, setFavorite: FavoriteSetter }

export type AppTimezone = Immutable<{
    utc: string,
    format: number,
    zone: number
}>

export type AppCookies = Pick<MiddlewareProps, 'res'> & Immutable<{
    name: string,
    value: unknown,
    options: CookieSerializeOptions
}>

interface AppContextProps<T = {}> extends Context<{ [K in keyof T]?: T[K] }> {
    sportContext: {
        activeSport: Required<{ id: number, name: string }>,
        setActiveSport: SetState<object>
    },
    tournamentContext: {
        activeTournament: Required<{ id: number, name: string }>,
        setActiveTournament: SetState<object>
    },
    gameContext: {
        activeGame: string,
        setActiveGame: SetState<string>
    },
    tabContext: {
        activeTab: TABS_TYPES | null,
        setActiveTab: SetState<TABS_TYPES> | SetState<null>,
    },
    favoriteContext: {
        favorite: [] | never[],
        setFavorite: SetState<[]> | SetState<never[]>
    },
    timezoneContext: {
        timezone: number | null,
        setTimezone: SetState<number> | SetState<null>
    },
    timeContext: {
        time: string
        setTime: SetState<string>
    },
}

export default AppContextProps