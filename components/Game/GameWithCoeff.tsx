import Image from "next/image";
import {useAppContext} from "@/context";
import Link from 'next/dist/client/link';
import IconFavorite from "@/components/Translation/IconFavorite";
import type {FunctionComponent} from 'react';
import type {GameWithCoeffType} from "@/types/games";

const GameWithCoeff: FunctionComponent<GameWithCoeffType> =
    ({
         id,
         href,
         images,
         participants,
         startHours,
         startDate,
         coeff,
     }) => {
        const {gameContext: {setActiveGame}}: any = useAppContext()
        const name = participants[0].name + '-' + participants[1].name

        return (
            <div className="translation__match">
                <Link href={href}>
                    <a className="translation__inner" onClick={() => setActiveGame(name)}>
                        <div className="translation__column">
                            <span>{startHours}</span>
                            <span className="grey">{startDate}</span>
                        </div>
                        <div className="translation__column">
                            <span>
                                <Image width={18} height={18} src={images[0]} alt="image"/>
                                &nbsp;<span children={participants[0].name}/>
                            </span>
                            <span>
                                <Image width={18} height={18} src={images[1]} alt="image"/>
                                &nbsp;<span children={participants[1].name}/>
                             </span>
                        </div>
                        <div className="translation__column">
                            {coeff ? coeff.map(({key, value}) => {
                                    return <div className="translation__scores">
                                        <span className="grey">{key}</span>
                                        &nbsp;
                                        <span>{value}</span>
                                    </div>
                                }) :
                                [2, 2, 2].map(el => {
                                    return <div key={Math.random()} className="translation__scores shimmer">
                                        <span className="grey" children={el}/>
                                        &nbsp;<span children={el}/>
                                    </div>
                                })}
                        </div>
                    </a>
                </Link>
                <IconFavorite id={id}/>
            </div>
        );
    };

export default GameWithCoeff;