import Image from "next/image";
import type {FunctionComponent} from 'react';
import type {GameWithScoreSetType} from "@/types/games";
import {ACTIVE} from "@/utils/constants";
import IconFavorite from "@/components/Translation/IconFavorite";
import LinkWrapper from "HOC/LinkWrapper";

const GameWithScoreSet: FunctionComponent<GameWithScoreSetType> =
    ({
         id,
         href,
         images,
         participants,
         periodPostfix,
         leader,
         activePeriod,
         score,
         periods,
     }) => {
        return (
            <div className="translation__match">
                <LinkWrapper href={href}>
                    <a className="translation__inner">
                        <div className="translation__column">
                            <span className="orange">{activePeriod} {periodPostfix}</span>
                        </div>
                        <div className="translation__column">
                                 <span>
                            <Image width={18} height={18} src={images[0]} alt="image"/>
                            <span>{participants[0].name}</span>
                        </span>
                            <span>
                            <Image width={18} height={18} src={images[1]} alt="image"/>
                            <span>{participants[1].name}</span>
                        </span>
                        </div>
                        <div className="translation__column">
                            <span className={`${leader === 0 ? ACTIVE : ''}`}/>
                            <span className={`${leader === 1 ? ACTIVE : ''}`}/>
                        </div>
                        <div className="translation__column">
                            <div className="translation__score">
                                <span>{score[0]}</span>
                                <span>{score[1]}</span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">{periods[0][1]}<sup>10</sup></span>
                                <span className="grey">6<sup>8</sup></span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">7<sup>10</sup></span>
                                <span className="grey">6<sup>8</sup></span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">7<sup>10</sup></span>
                                <span className="grey">6<sup>8</sup></span>
                            </div>
                            <div className="translation__score">
                                <span className="grey">7<sup>10</sup></span>
                                <span className="grey">6<sup>8</sup></span>
                            </div>
                        </div>
                    </a>
                </LinkWrapper>
                <IconFavorite id={id}/>
            </div>
        );
    };

export default GameWithScoreSet;