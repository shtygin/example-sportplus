import IconFavorite from "@/components/Translation/IconFavorite";
import moment from "@/moment";

const Countdown = ({name, start}: { name: string, start: string }) => {
    // @ts-ignore
    const {days, hours, minutes} = moment.countdown(Date.now() + 3 * 60 * 60 * 24 * 1000)
    return <section className="translation__block">
        <div className="translation__champ">
            <h3 className="title-sm">{name}</h3>
            <IconFavorite id={1}/>
        </div>
        <div className="translation__condition">
            <span className="title">{moment(start).format('Do MMMM, h:mm')}</span>
            <div className="translation__count-down">
                <div className="translation__cd">
                    <span>{days}</span>
                    <span className="grey text">Дня</span>
                </div>
                <div className="translation__cd">
                    <span>{hours}</span>
                    <span className="grey text">Часов</span>
                </div>
                <div className="translation__cd">
                    <span>{minutes}</span>
                    <span className="grey text">Минут</span>
                </div>
            </div>
        </div>
    </section>

};

export default Countdown;