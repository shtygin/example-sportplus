import GameWithShimmerCoeff from "@/components/Game/GameWithShimmerCoeff";
import Champ from "@/components/Broadcast/Champ";

const Other = () => {
    return  <section className="translation__block">
            <Champ name={'Другие матчи этого турнира'} id={123123}/>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <div className="translation__date">Сегодня</div>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <div className="translation__date">25 августа</div>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <GameWithShimmerCoeff/>
            <div className="translation__link">
                <a href="">Показать ещё</a>
            </div>
        </section>
};

export default Other;