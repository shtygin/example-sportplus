import IconFavorite from "@/components/Translation/IconFavorite";

const Ended = ({name, id}: {name: string, id: number}) => {
    return <section className="translation__block">
            <div className="translation__champ">
                <h3 className="title-sm">{name}</h3>
                <IconFavorite id={id}/>
            </div>
            <div className="translation__condition _icon-success">
                <span className="title">Трансляция завершена</span>
            </div>
        </section>
};

export default Ended;