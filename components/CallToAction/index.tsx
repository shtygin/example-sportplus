import {useTranslation} from "next-i18next";

const CallToAction = () => {
    const {t} = useTranslation('common')
    return (
        <div className="page__text text grey">
            {t('CALL_TO_ACTION')}
        </div>
    );
};

export default CallToAction;