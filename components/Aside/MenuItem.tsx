import {useAppContext} from "@/context";
import {ACTIVE} from "@/utils/constants";

import type {SportProps} from "@/types/props";
import Link from "next/link";

const MenuItem = ({
                      id, name, total,
                      sportURI, tournaments, onSportClick
                  }: SportProps) => {
    const {
        sportContext: {activeSport, setActiveSport},
        tournamentContext: {setActiveTournament}
    }: any = useAppContext()

    return !total ? null : <>
        <li
            className={`aside__item ${activeSport === name ? ACTIVE : ''}`}
            onClick={() => {
                setActiveSport({id, name: name.toLowerCase()})
                onSportClick()
            }}>
            <Link href={{
                pathname: `/[sport]`,
                query: {sport: sportURI},
            }}>
                <a className="aside__link _icon-soccer-ball" children={name}/>
            </Link>
            <div children={total} className="aside__score _icon-chevron-down"/>
        </li>
        <ul className={`aside__sub-list ${activeSport === name ? ACTIVE : ''}`}>
            {tournaments!.map(({id, name, total, tournamentURI}) => {
                return <li
                    key={id}
                    className="aside__sub-item"
                    onClick={() => setActiveTournament(name)}
                >
                    <Link href={{
                        pathname: '/[sport]/[tournament]',
                        query: {sport: sportURI, tournament: tournamentURI},
                    }}>
                        <a className="aside__sub-link">
                            <span>{name}</span>
                            <span>{total}</span>
                        </a>
                    </Link>
                </li>
            })}
        </ul>
    </>
};

export default MenuItem;