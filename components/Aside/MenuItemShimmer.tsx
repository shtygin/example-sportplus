import type {FunctionComponent} from 'react';

const MenuItemShimmer: FunctionComponent = () => {
    return <>
        {Array.from(Array(10), (x, i) => i).map(el => {
            return <li key={el} className="aside__item shimmer__aside">
                <div className="shimmer shimmer__aside-icon" children={'c'}/>
                <div className="shimmer shimmer__aside-item" children={'c'}/>
                <div className="shimmer shimmer__aside-icon" children={'c'}/>
            </li>
        })}
    </>
};

export default MenuItemShimmer;