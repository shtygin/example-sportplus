import dynamic from "next/dynamic";
import {useAppContext} from "@/context";
import Link from "next/dist/client/link";
import {useTranslation} from "next-i18next";
import {useTranslations} from "@/useRequest";
import type {FunctionComponent, MouseEvent} from "react";
import {useEffect, useRef, useState} from "react";
import MenuItem from "@/components/Aside/MenuItem";
import {ACTIVE, MODAL_OPEN, PAGES_ROUTES, SCROLL_OPTIONS} from "@/utils/constants";
import type {PopupComponent} from "@/types/components";
import {useRouter} from "next/router";

const Popup: PopupComponent = dynamic(() => import("@/components/Popup"))

const AsideMenu: FunctionComponent = () => {
    const {asPath} = useRouter()
    const [open, setOpen] = useState<boolean>(false)
    const {sportContext: {activeSport, setActiveSport}, timeContext: {time}} = useAppContext()

    const onSportClick = (
        event: MouseEvent<HTMLLIElement, MouseEvent>,
        name: string
    ) => {
        if (activeSport !== name) {
            setActiveSport(name)
            return;
        }

        document.body.classList.remove(MODAL_OPEN)
        open && document.body.classList.add(MODAL_OPEN)
    }

    const {data} = useTranslations()
    const {t} = useTranslation(['categories', 'common'])

    const [switchActive, setSwitchActive] = useState<boolean>(false)

    const popupRef = useRef<null | HTMLDivElement>(null)
    const listRef = useRef<null | HTMLLIElement>(null)

    const onSwitchClick = () => {
        setSwitchActive(!switchActive)
    }

    const onPopupClick = () => {
        setOpen(true)
    }

    const handlerEscPress = (event: { keyCode: number }) => {
        if (event.keyCode === 27) setOpen(false)
    }

    const handlerClickOutside = (event: (MouseEvent & Node) | any) => {
        const {target} = event
        !popupRef.current?.contains(target) && setOpen(false)
    }

    useEffect(() => {
        listRef.current?.scrollIntoView(SCROLL_OPTIONS)
    }, [onPopupClick])

    useEffect(() => {
        document.addEventListener("keydown", handlerEscPress, false);
        document.addEventListener("mousedown", handlerClickOutside);
        return () => {
            document.removeEventListener("mousedown", handlerClickOutside);
            document.removeEventListener("keydown", handlerEscPress, false);
        };
    }, []);

    useEffect(() => {
        document.body.classList.remove(MODAL_OPEN)
        open && document.body.classList.add(MODAL_OPEN)
    }, [open])

    return <aside className="page__aside aside">
        <nav className="aside__inner">
            <ul className="aside__list">
                <li className="aside__item aside__item--title">
                    <span>{t('common:TRANSLATIONS')}</span>
                </li>
                <li className="aside__item switch">
                    <div className="switch__inner" onClick={onSwitchClick}>
                        <div className={`switch__item ${switchActive ? ACTIVE : ''}`}/>
                        <span>Live</span>
                    </div>
                    <div className="aside__time " onClick={onPopupClick} children={time}/>
                </li>
                <li className="aside__item aside__item--link">
                    <Link href={PAGES_ROUTES['FAVORITE'].route}>
                        <a className={`aside__link _icon-star-off 
                            ${asPath === PAGES_ROUTES['FAVORITE'].route ? ACTIVE : ''}`}
                        >
                            {t('common:FAVORITE')}</a>
                    </Link>
                </li>
                {/*@ts-ignore*/}
                {data?.map((tr) => {
                    return <MenuItem
                        {...tr}
                        onSportClick={(e) => onSportClick(e, tr.name)}
                        total={switchActive ? tr.totalLive : tr.total}
                        tournaments={switchActive ? tr.tournamentsLive : tr.tournaments}
                        key={tr.id}/>
                })}
            </ul>
        </nav>
        <Popup
            popupRef={popupRef}
            listRef={listRef}
            setOpen={setOpen}
            open={open}
        />
    </aside>
}

export default AsideMenu