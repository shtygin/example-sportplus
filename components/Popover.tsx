import type {FunctionComponent} from "react";

const Popover: FunctionComponent = () => {
    return (
        <div className="popover">
            <div className="popover__inner">
                <div className="popover__margin">Средняя маржа: <b>4.30%</b><b>1.92*</b></div>
                <div className="popover__text">*Примерный кф. для исхода с 50% вероятностью с учетом маржи.
                    Для сравнения, при нулевой марже кф. равен 2.0.
                </div>
            </div>
        </div>
    );
};

export default Popover;