import moment from "@/moment";
import {useEffect} from "react";
import type {NextRouter} from "next/router";
import {useRouter} from "next/router";
import {useAppContext} from "@/context";
import Button from "@/components/Button/button";
import {ACTIVE, TIMEZONES} from "@/utils/constants";
import type {PopupComponent} from "@/types/components";

const Popup: PopupComponent = ({
                                   popupRef, listRef,
                                   open, setOpen
                               }) => {
    const {locale}: NextRouter = useRouter()
    const {
        timezoneContext: {timezone, setTimezone},
        timeContext: {time, setTime},
    }: any = useAppContext()

    const onTimezoneClick = (format: number) => {
        localStorage.setItem('timezone', JSON.stringify(format))
        setTimezone(JSON.parse(localStorage.getItem('timezone') ?? ''))
    }

    const onClick = () => {
        setOpen(false)
    }

    useEffect(() => {
        moment.locale(locale)
        timezone && setTime(moment().utcOffset(timezone * -1).format())
    }, [timezone, setTimezone])

    return (
        <div className={`popup ${open ? 'popup_open' : ''} `}>
            <div className="popup__bg"/>
            <div className="popup__wrapper">
                <div className="popup__content" ref={popupRef}>
                    <button className="popup__close _icon-close" onClick={onClick}/>
                    <h2 className="popup__title sub-title">Часовой пояс</h2>
                    <div className="popup__time">Время {time}</div>
                    <div className="popup__block">
                        <ul className="popup__list">
                            {TIMEZONES.map(({utc, format}) => {
                                return <li
                                    key={utc}
                                    onClick={() => onTimezoneClick(format)}
                                    className={`popup__item ${format === timezone ? ACTIVE : ''}`}
                                    ref={format === timezone ? listRef : null}
                                >
                                    <span>{utc}</span>
                                </li>
                            })}
                        </ul>
                    </div>
                    <Button
                        className={"popup__btn btn btn_orange"}
                        onClick={() => setOpen(false)}
                        text={'Выбрать'}
                    />
                </div>
            </div>
        </div>
    );
};

export default Popup;