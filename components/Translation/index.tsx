import {type FunctionComponent, Fragment} from 'react';
import {useTranslation} from "next-i18next";
import GameWithCoeff from "@/components/Game/GameWithCoeff";
import LinkWrapper from 'HOC/LinkWrapper';
import BannerSmall from '../Banners/BannerSmall';
import CallToAction from '../CallToAction';
import Tabs from "@/components/Tabs";
import type {SportProps} from "@/types/props";
import moment from "@/moment";
import SportPageShimmer from "@/pages/[sport]/shimmer";
import TabsShimmer from "@/components/Tabs/Shimmer";
import {useSportTab} from "@/useRequest";
import {useAppContext} from "@/context";

const Translation: FunctionComponent = () => {
    const {data} = useSportTab()
    const {tabContext: {activeTab}} = useAppContext()
    const {t} = useTranslation('home-page')

    if (!data || !Array.isArray(data[activeTab])) {
        return <Fragment>
            <TabsShimmer/>
            <SportPageShimmer/>
        </Fragment>
    }

    return <Fragment>
        <Tabs title={t('TITLE')} tabs={Object.keys(data)}/>
        {data[activeTab].map((sport: SportProps) => {
            return <Fragment key={sport.id}>
                <section className="translation__block">
                    <div className="translation__head">
                        <LinkWrapper href={sport.alias ?? sport.id}>
                            <h2 className="translation__sub-title title">{sport.name}</h2>
                        </LinkWrapper>&nbsp;
                        <span className="grey">12</span>
                    </div>
                    {sport.tournaments.map(({name, id, matches}) => {
                        return <Fragment key={id}>
                            <div className="translation__champ">
                                <LinkWrapper href={sport.alias ?? sport.id} children={name}/>&nbsp;
                                <i className="translation__icon _icon-star-off"/>
                            </div>
                            {matches.map(({home, away, id, start}: any) => {
                                return <GameWithCoeff
                                    key={id} id={id}
                                    href={id.toString()}
                                    participants={[{
                                        id: home.id,
                                        name: home.name
                                    }, {
                                        id: away.id,
                                        name: away.name
                                    }]}
                                    images={[`/onexbet/${home.id}`, `/onexbet/${away.id}`]}
                                    startHours={moment(start).format()}
                                    startDate={moment(start).format('DD.MM')}
                                    coeff={null}
                                />
                            })}
                        </Fragment>
                    })}
                    {sport.has_more && <div
						className="translation__link"
						children={<a children={t('SHOW_MORE')}/>}
					/>}
                </section>
                <BannerSmall/>
            </Fragment>
        })}
        <CallToAction/>
    </Fragment>
};

export default Translation;

