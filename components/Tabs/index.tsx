import {Fragment} from 'react';
import {useAppContext} from "@/context";
import {useTranslation} from "next-i18next";
import {ACTIVE} from "@/utils/constants";
import Button from "@/components/Button/button";
import type {TabsComponent} from "@/types/components";

const Tabs: TabsComponent = ({title, tabs}): JSX.Element => {
    const {tabContext: {activeTab, setActiveTab}}: any = useAppContext()
    const {t} = useTranslation('tabs')
    console.log(tabs)
    tabs && setActiveTab(tabs[0])

    return <Fragment>
        <h1 className={"translation__title title"}>{t(title)}</h1>
        <div className={"translation__tabs tabs"}>
            {tabs && tabs.map((tab) => {
                const className = `tabs__item text btn ${activeTab === tab ? ACTIVE : ''}`
                return <Button
                    className={className}
                    onClick={() => setActiveTab(tab)}
                    key={tab}
                    text={t(tab)}/>
            })}
        </div>
    </Fragment>
};

export default Tabs;