import React from 'react';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";

const Copyright = () => {
    const {t} = useTranslation(['copyright-page'])
    return <section className="copy__block">
        <h1 className="copy__title title-sm">{t('TITLE')}</h1>
        <div className="copy__body text">
            <p>
                {t('MAIN_INFO.START')}
                &nbsp;<a href="mailto:ru.sportplus.live">ru.sportplus.live</a>&nbsp;
                {t('MAIN_INFO.END')}
            </p>
            <p>{t('TECH')}</p>
            <div className="copy__note">
                <p>
                    {t('ADMIN_POSITION.HEADER.START')}
                    &nbsp;<a href="mailto:ru.sportplus.live">ru.sportplus.live</a>&nbsp;
                    {t('ADMIN_POSITION.HEADER.END')}
                </p>
                <ul>
                    <li>{t('ADMIN_POSITION.LI1')}</li>
                    <li>{t('ADMIN_POSITION.LI2')}</li>
                    <li>{t('ADMIN_POSITION.LI3')}</li>
                    <li>{t('ADMIN_POSITION.LI4')}</li>
                </ul>
                <p>
                    {t('RIGHTS_BREAK.START')}
                    &nbsp;<a href="mailto:admin@sportplus.live">ru.sportplus.live</a>&nbsp;
                    {t('RIGHTS_BREAK.END')}
                    &nbsp;<a href="mailto:admin@sportplus.live">admin@sportplus.live</a>
                </p>
                <p>{t('ABOUT_COMPLAINT')}</p>
            </div>
            <p><b>{t('COMPLAINT.HEADER')}</b></p>
            <ol>
                <li>{t('COMPLAINT.STEP1')}</li>
                <li>{t('COMPLAINT.STEP2')}</li>
                <ul>
                    <li>{t('COMPLAINT.STEP3')}</li>
                    <li>{t('COMPLAINT.STEP4')}</li>
                    <li>{t('COMPLAINT.STEP5')}</li>
                </ul>
                <li>{t('COMPLAINT.STEP6')}</li>
                <ul>
                    <li>{t('COMPLAINT.STEP7')}</li>
                    <li>{t('COMPLAINT.STEP8')}</li>
                </ul>
                <li>{t('COMPLAINT.STEP9')}</li>
                <ul>
                    <li>
                        {t('COMPLAINT.STEP10.START')}
                        &nbsp;<a href="mailto:ru.sportplus.live">ru.sportplus.live</a>&nbsp;
                        {t('COMPLAINT.STEP10.END')}
                    </li>
                    <li>{t('COMPLAINT.STEP11')}</li>
                </ul>
            </ol>
        </div>
        <div className="copy__email text">
            {t('MAILTO')}&nbsp;
            <a href="mailto:admin@sportplus.live">admin@sportplus.live</a>
        </div>
    </section>
};

export const getStaticProps = async ({locale}: { locale: string }) => {
    return {
        props: {
            ...await serverSideTranslations(locale,
                ['categories', 'tabs', 'common', 'copyright-page']
            ),
        },
    }
}

export default Copyright;