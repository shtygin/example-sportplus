import {Fragment} from "react";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import type {NextPage} from "next";

const Bonuses: NextPage = (props) => {
    return <Fragment>
        <h1 className="translation__title title-b">Бонусы букмекеров</h1>
        <section className="bk__block">
            <p>Список только лучших букмекерских контор для ставок России, каждый букмекер как минимум имеет
                одну из двух особенностей:</p>
            <ul>
                <li>большое количество прямых видео трансляций матчей, а значит они идеально подходят для лайв
                    ставок;
                </li>
                <li>высокие коэффициенты (низкая маржа), что значит ставки в этих букмекерских конторах наиболее
                    выгодны.
                </li>
            </ul>
        </section>
        <div className="bk__select select">
            <div className="select__head">Россия</div>
            <div className="select__i">
                <ul className="select__list">
                    <li className="select__item active">Россия</li>
                    <li className="select__item">Украина</li>
                    <li className="select__item">Казахстан</li>
                    <li className="select__item">Беларусь</li>
                    <li className="select__item">СНГ</li>
                </ul>
            </div>
        </div>
    </Fragment>
}

export const getStaticProps = async ({locale}: { locale: string }): Promise<any> => {
    return {
        props: {
            ...await serverSideTranslations(locale, ["categories", "tabs", "common"]),
        },
    }
}

export default Bonuses