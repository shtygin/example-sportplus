import BannerSmallShimmer from "@/components/Banners/BannerSmallShimmer";
import GameShimmerScore from '@/components/Game/GameWithShimmerScoreHalf';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {type NextRouter, useRouter} from 'next/router'
import type {GetStaticProps} from "next";

const Tournament = () => {
    const {query: {name}}: NextRouter = useRouter()

    return <>
        <BannerSmallShimmer/>
        <section className="translation__block">
            <div className="translation__champ">
                <h1 className="title-sm">{name}</h1>
                <i className="translation__icon _icon-star-off shimmer"/>
            </div>
            <GameShimmerScore/>
            <GameShimmerScore/>
            <GameShimmerScore/>
            <GameShimmerScore/>
            <div className="translation__date">Сегодня</div>
            <GameShimmerScore/>
            <GameShimmerScore/>
            <div className="translation__date">25 августа</div>
            <GameShimmerScore/>
            <GameShimmerScore/>
        </section>
    </>
}

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            ...await serverSideTranslations(locale as string, ["categories", "tabs", "common"]),
        },
    }
}

export async function getStaticPaths() {
    return {
        paths: [{params: {sport: '', tournament: ''}}],
        fallback: true
    };
}

export default Tournament
