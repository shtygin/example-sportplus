import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import type {GetStaticProps} from "next";
import dynamic from "next/dynamic";
import type {ComponentType} from "react";

const Champ: ComponentType<{ readonly id?: any; readonly name?: any }> = dynamic(() => import("@/components/Broadcast/Champ"));
const Forbidden: ComponentType<{ readonly id?: any; readonly name?: any }> = dynamic(() => import("@/components/Broadcast/Forbidden"));
const Countdown: ComponentType<{ readonly id?: any; readonly name?: any }> = dynamic(() => import("@/components/Broadcast/Countdown"));
const Ended: ComponentType<{ readonly id?: any; readonly name?: any }> = dynamic(() => import("@/components/Broadcast/Ended"));
const Share: ComponentType<{ readonly id?: any; readonly name?: any }> = dynamic(() => import("@/components/Broadcast/Share"));
const Other: ComponentType<{ readonly id?: any; readonly name?: any }> = dynamic(() => import("@/components/Broadcast/Other"));

const BannerSmallShimmer = dynamic(() => import("@/components/Banners/BannerBigShimmer"));
const BannerSmall = dynamic(() => import("@/components/Banners/BannerSmall"));

const Broadcast = () => {
    return <>
        <BannerSmallShimmer/>
        <section className="translation__block">
            <Champ id={1} name={'asdasdas - dddddddddd'}/>
            <div className="translation__stream">
                <div className="translation__counter text">
                    <span>57'</span>
                    <span>2:2</span>
                    <span>(1:0)</span>
                </div>
                <div className="translation__broadcast">
                    <div/>
                </div>
                <div className="tabs tabs--sm">
                    <button className="tabs__item text btn active" type="button">1</button>
                    <button className="tabs__item text btn" type="button">2</button>
                    <button className="tabs__item text btn" type="button">3</button>
                </div>
            </div>
        </section>
        <Forbidden name={'Ливерпуль - Ньюкасл Юнайтед'} id={1}/>
        <Countdown start={Date.now() + 3 * 60 * 60 * 24 * 1000} name={'Ливерпуль - Ньюкасл Юнайтед'} id={1}/>
        <Ended name={'Ливерпуль - Ньюкасл Юнайтед'} id={1}/>
        <Share/>
        <Other/>
    </>
}

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            ...await serverSideTranslations(locale as string, ["broadcast"]),
        },
    }
}

export async function getStaticPaths() {
    return {
        paths: [{params: {sport: '', tournament: '', broadcast: ''}}],
        fallback: true
    };
}

export default Broadcast
