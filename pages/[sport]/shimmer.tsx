import {Fragment} from "react";
import GameShimmerCoeff from "@/components/Game/GameWithShimmerScoreHalf";
import BannerSmallShimmer from "@/components/Banners/BannerSmallShimmer";

const SportPageShimmer = () => <Fragment>
    {Array(3).fill(0).map(_ => {
        return <Fragment key={Math.random()}>
            <GameShimmerCoeff/>
            <BannerSmallShimmer/>
        </Fragment>
    })}
</Fragment>

export default SportPageShimmer