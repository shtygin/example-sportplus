import type {COMMON_TYPES, PAGES_STATIC_ROUTES_TYPES, PAGES_STATIC_TYPES, TABS_TYPES} from "@/types/unions";
import type {AppTimezone} from "@/types/context";
import type {ScrollProps} from "@/types/props";

export const API_ORIGIN = process.env["NEXT_PUBLIC_API_TEST"] === 'true'
    ? process.env["NEXT_PUBLIC_API_ORIGIN"]
    : process.env["NEXT_PUBLIC_APP_DEVELOPMENT_ORIGIN"]

export const TIMEZONES: AppTimezone[] = [
    {utc: 'UTC -11:00', format: 660 / 60, zone: -660},
    {utc: 'UTC -10:00', format: 600 / 60, zone: -600},
    {utc: 'UTC -09:30', format: 570 / 60, zone: -570},
    {utc: 'UTC -09:00', format: 540 / 60, zone: -540},
    {utc: 'UTC -08:30', format: 510 / 60, zone: -510},
    {utc: 'UTC -08:00', format: 480 / 60, zone: -480},
    {utc: 'UTC -07:00', format: 420 / 60, zone: -420},
    {utc: 'UTC -06:00', format: 360 / 60, zone: -360},
    {utc: 'UTC -05:00', format: 300 / 60, zone: -300},
    {utc: 'UTC -04:30', format: 270 / 60, zone: -270},
    {utc: 'UTC -04:00', format: 240 / 60, zone: -240},
    {utc: 'UTC -03:30', format: 210 / 60, zone: -210},
    {utc: 'UTC -03:00', format: 180 / 60, zone: -180},
    {utc: 'UTC -02:30', format: 150 / 60, zone: -150},
    {utc: 'UTC -02:00', format: 120 / 60, zone: -120},
    {utc: 'UTC -01:00', format: 60 / 60, zone: -60},
    {utc: 'UTC +00:00', format: .1 / 60, zone: 0},
    {utc: 'UTC +01:00', format: -60 / 60, zone: 60},
    {utc: 'UTC +02:00', format: -120 / 60, zone: 120},
    {utc: 'UTC +03:00', format: -180 / 60, zone: 180},
    {utc: 'UTC +03:30', format: -210 / 60, zone: 210},
    {utc: 'UTC +04:00', format: -240 / 60, zone: 240},
    {utc: 'UTC +04:30', format: -270 / 60, zone: 270},
    {utc: 'UTC +05:00', format: -300 / 60, zone: 300},
    {utc: 'UTC +05:30', format: -330 / 60, zone: 330},
    {utc: 'UTC +05:45', format: -345 / 60, zone: 345},
    {utc: 'UTC +06:00', format: -360 / 60, zone: 360},
    {utc: 'UTC +06:45', format: -405 / 60, zone: 405},
    {utc: 'UTC +07:00', format: -420 / 60, zone: 420},
    {utc: 'UTC +07:30', format: -450 / 60, zone: 450},
    {utc: 'UTC +08:00', format: -480 / 60, zone: 480},
    {utc: 'UTC +08:30', format: -510 / 60, zone: 510},
    {utc: 'UTC +08:45', format: -525 / 60, zone: 525},
    {utc: 'UTC +09:00', format: -540 / 60, zone: 540},
    {utc: 'UTC +09:30', format: -570 / 60, zone: 570},
    {utc: 'UTC +09:45', format: -585 / 60, zone: 585},
    {utc: 'UTC +10:00', format: -600 / 60, zone: 600},
    {utc: 'UTC +10:30', format: -630 / 60, zone: 630},
    {utc: 'UTC +11:00', format: -660 / 60, zone: 660},
    {utc: 'UTC +11:30', format: -690 / 60, zone: 690},
    {utc: 'UTC +12:00', format: -720 / 60, zone: 720},
    {utc: 'UTC +12:30', format: -750 / 60, zone: 750},
    {utc: 'UTC +12:45', format: -765 / 60, zone: 765},
    {utc: 'UTC +13:00', format: -780 / 60, zone: 780},
    {utc: 'UTC +13:30', format: -810 / 60, zone: 810},
    {utc: 'UTC +13:45', format: -825 / 60, zone: 825},
    {utc: 'UTC +14:00', format: -840 / 60, zone: 840},
]

export const DEFAULT_TIMEZONE = TIMEZONES.filter(({zone}) =>
    zone === new Date().getTimezoneOffset())[0].format;

export const SCROLL_OPTIONS: ScrollProps = {
    behavior: "smooth",
    block: "center"
} as const

export const ACTIVE: Lowercase<COMMON_TYPES & 'ACTIVE'> = 'active'
export const OPEN: Lowercase<COMMON_TYPES & 'OPEN'> = 'open'
export const MODAL_OPEN: Lowercase<COMMON_TYPES & 'modal-open'> = 'modal-open'
export const ASIDE_OPEN: Lowercase<COMMON_TYPES & 'aside-open'> = 'aside-open'

const LIVE: TABS_TYPES & 'LIVE' = 'LIVE'
const TODAY: TABS_TYPES & 'TODAY' = 'TODAY'
const LATER: TABS_TYPES & 'LATER' = 'LATER'
export const TABS: TABS_TYPES[] = [LIVE, TODAY]

export const HOME: PAGES_STATIC_TYPES & 'HOME' = 'HOME'
export const BOOKMAKERS: PAGES_STATIC_TYPES & 'BOOKMAKERS' = 'BOOKMAKERS'
export const BONUSES: PAGES_STATIC_TYPES & 'BONUSES' = 'BONUSES'
export const SUPPORT: PAGES_STATIC_TYPES & 'SUPPORT' = 'SUPPORT'
export const COPYRIGHT: PAGES_STATIC_TYPES & 'COPYRIGHT' = 'COPYRIGHT'
export const FAVORITE: PAGES_STATIC_TYPES & 'FAVORITE' = 'FAVORITE'
export const PAGES_ROUTES: Record<PAGES_STATIC_TYPES, {
    route: PAGES_STATIC_ROUTES_TYPES,
    name: PAGES_STATIC_TYPES
}> = {
    HOME: {route: '/', name: HOME},
    BOOKMAKERS: {route: '/bookmakers/promo/', name: BOOKMAKERS},
    BONUSES: {route: '/bonuses/', name: BONUSES},
    SUPPORT: {route: '/support/', name: SUPPORT},
    COPYRIGHT: {route: '/copyright/', name: COPYRIGHT},
    FAVORITE: {route: '/favorite/', name: FAVORITE}
}

