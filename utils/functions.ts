import type {NextWebVitalsMetric} from 'next/app'

export const getLanguage = () => (navigator && (navigator.languages && navigator.languages.length && navigator.languages[0]) || navigator.language) || 'en';

export const reportWebVitals: Function = (metric: NextWebVitalsMetric) => {
    switch (metric.name) {
        case 'FCP':
            console.log('FCP', metric)
            break
        case 'LCP':
            console.log('LCP', metric)
            break
        case 'CLS':
            console.log('CLS', metric)
            break
        case 'FID':
            console.log('FID', metric)
            break
        case 'TTFB':
            console.log('TTFB', metric)
            break
        case 'Next.js-hydration':
            console.log('hydration', metric)
            break
        case 'Next.js-route-change-to-render':
            console.log('route-change to render', metric)
            break
        case 'Next.js-render':
            console.log('render', metric)
            break
        default:
            break
    }
}

export const fetcher = async (url: RequestInfo): Promise<JSON> | never => {
    const res = await fetch(url)
    if (!res.ok) throw new Error(res.statusText)
    return res.json()
}

export const isSubdomain = (locales): void => {
    // const d = process.env["NEXT_PUBLIC_d_LIST"]!
    const locale = process.env["NEXT_PUBLIC_LOCALES_LIST"] || getLanguage()
    const domainPreffix = location.hostname.split('.')[0]
    if (!locales.includes(domainPreffix)) {
        const [originStart, originEnd] = location.href.split('://')
        location.href = originStart + '://' + locale + '.' + originEnd
    }
}