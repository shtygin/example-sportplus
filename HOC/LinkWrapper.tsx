import type {LinkProps} from "next/dist/client/link";
import Link from "next/dist/client/link";
import type {ExtendProps} from "@/types/components";
import type {FunctionComponent} from "react";

const LinkWrapper: FunctionComponent<ExtendProps<LinkProps>> = (props) => {
    return <Link {...props} passHref={true} locale={false}>
        {props.children && props.children}
    </Link>
};

export default LinkWrapper;
