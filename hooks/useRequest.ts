import type {Middleware, SWRResponse} from "swr"
import useSWR from "swr"
import type {NextRouter} from "next/router";
import {useRouter} from "next/router";
import {fetcher} from "@/utils/functions";
import {API_ORIGIN} from "@/utils/constants";
import type {SportProps, TranslationsProps} from "@/types/props";
import {useMemo} from "react";
import {useAppContext} from "@/context";

export const logger: Middleware = (useSWRNext) => {
    return (key, fetcher, config) => {
        const extendedFetcher = (...args: any[]) => {
            // console.log('useRequest:', key, args)
            return fetcher(...args)
        }
        return useSWRNext(key, extendedFetcher, config)
    }
}

export const useTranslations = (): TranslationsProps => {
    const {locale}: NextRouter = useRouter()
    const endpoint = `${API_ORIGIN}/v3/${locale}/sidebar.json`;
    // @ts-ignore
    const {data, error}: SWRResponse<SportProps[], Error> = useSWR(endpoint, fetcher)

    data?.forEach(sport => {
        sport.tournamentsLive = []
        sport.totalLive = 0

        if (!sport.sportURI) sport.sportURI = sport.alias ?? sport.id.toString()
        if (!sport.total) sport.total =
            sport.tournaments.reduce((total, {cs}, i) => {
                const {not_started, live} = cs

                if (!sport.tournaments[i].tournamentURI) {
                    const {alias, id, cs} = sport.tournaments[i]
                    const {not_started, live} = cs

                    sport.tournaments[i].tournamentURI = alias ?? id.toString()

                    if (!sport.tournaments[i].total) sport.tournaments[i].total = 0

                    sport.tournaments[i].total += not_started ?? 0
                    sport.tournaments[i].total += live ?? 0
                }

                total += (not_started ?? 0) + (live ?? 0)
                return total
            }, 0)

        sport.tournamentsLive = sport.tournaments.filter(tournament => {
            sport.totalLive += tournament.cs.live ? tournament.cs.live : 0
            return tournament.cs.live
        })
    })

    return {
        data: data ?? null,
        isLoading: !error && !data,
        isError: error ?? false
    }
}

export const useSportTranslations = (): TranslationsProps => {
    // const {locale}: NextRouter = useRouter()
    // const endpoint = `${API_ORIGIN}/${locale}/sidebar.json/${id}`;
    //
    // const {data, error}: SWRResponse<JSON, never> = useSWR(endpoint, fetcher, {
    //     use: [logger],
    //     shouldRetryOnError: false
    // })

    // return {
    //     data: data ?? null,
    //     isLoading: !error && !data,
    //     isError: error ?? false
    // }


    return {
        data: [],
        isError: false,
        isLoading: true
    }
}

export const useSportTab = () => {
    const {locale}: NextRouter = useRouter()
    const {
        timezoneContext: {timezone},
        tabContext: {activeTab, setActiveTab},
        excludeContext: {excludeIds},
    } = useAppContext()

    const {type} = useMemo(() => ({
        type: activeTab,
        excludeIds,
    }), [])

    const endpoint = `${API_ORIGIN}/v3/${locale}/matches/index?offset=${60 * 60 * timezone}`

    const {data} = useSWR([endpoint, {type, excludeIds}], fetcher)

    const onClick = async () => {
        // await fetch(`${API_ORIGIN}/v3/${locale}/matches/more?`, {
        //     method: 'post',
        //     headers: {"Content-Type": "application/json"},
        //     body: JSON.stringify({type, excludeIds})
        // }).then(res => res.json())
        // mutate(endpoint, async () => {
        // console.log(cache.get(endpoint))
        // }).then(res => console.log('mutate', endpoint))
    }

    return {
        data: data ?? null,
    }
}

export const useTranslationsGroups = (): SportProps[][] => {
    const {data} = useTranslations()
    const groups: SportProps[][] = []

    if (!data) return [];

    // @ts-ignore
    for (let i = 0; !(i >= data.length); i += 4) {
        // @ts-ignore
        groups.push(data.slice(i, i + 4));
    }

    return groups
}