export declare type CommonKeys<T, U> = {
    [K in Extract<keyof T, keyof U>]: T[K]
}
export declare type UniqueKeys<T, U> = {
    [K in Exclude<keyof T, keyof U>]: T[K]
}
export declare type Filter<T, U> = T extends U ? T : never;
export declare type DiffKeys<T, U> = Omit<T, keyof U> & Omit<U, keyof T>
export declare type SameKeys<T, U> = Omit<T | U, keyof DiffKeys<T, U>>